var socket = io.connect("https://polar-garden-36679.herokuapp.com/",{
    transports: ['websocket']
});
r=0
tr=0
tred=tgreen=tblue=red=green=blue=150
color=0
function setup(){
	createCanvas(windowWidth,windowHeight)
	socket.on("update",function(data){
		board=data
		sx=undefined
		sy=undefined
		st=0
	}).on("colorchange",function(data){
		if(data[0]!==null&&data[0]>=0)tr+=PI
		if(data[0]===color||data[0]===null)color=data[1]
		tred=[20,150,225][data[1]%3]
		tgreen=[20,150,225][Math.floor(data[1]/3)%3]
		tblue=[20,150,225][Math.floor(data[1]/9)%3]
	})
}
function windowResized() { 
    resizeCanvas(windowWidth, windowHeight); 
}
draw=colorselect
function colorselect(){
	colorMode(HSB)
	strokeWeight(.25)
	background(30,30,30)
	colorMode(RGB)
	rectMode(CENTER)
	translate(width/2,height/2)
	scale(height/100)
	if(si>=0)st+=(2-st)*.3
	for(var i=0;i<27;i++){
		var selected=si==i
		fill([25,150,225][(i%3)],[25,150,225][(Math.floor(i/3)%3)],[25,150,225][(Math.floor(i/9)%3)])
		rect(10*floor(i/3)-40,10*(i%3)-10,7+selected*st,7+selected*st)
	}
}
function titlescreen(){
	colorMode(HSB)
	strokeWeight(.25)
	background(0,100,30)
	colorMode(RGB)
	textSize(100)
	textAlign(CENTER)
	for(var i=0;i<9;i++){
		fill(0)
		text("CELLSPACE".charAt(i),width/2-240+i*60,300)
		var y=Math.max(0,sin(i/4+millis()/300))*100
		fill(255-(floor(i/4)%2)*y*1.55,255-(floor(i/2)%2)*y*1.55,255-(i%2)*y*1.55)
		text("CELLSPACE".charAt(i),width/2-240+i*60,300-y)
		
		
	}
}
st=0
si=undefined
sx=sy=undefined
board=([...Array(7)]).map(x=>([...Array(7)]).map(x=>null))
function game(){
	r+=(tr-r)*.1
	rectMode(CENTER)
	background(red,green,blue)
	if(typeof red != "number")red=0
	if(typeof green != "number")green=0
	if(typeof blue != "number")blue=0
	red+=(tred-red)*.1
	green+=(tgreen-green)*.1
	blue+=(tblue-blue)*.1
	translate(width/2,height/2)
	scale(height/100)
	strokeWeight(.25)
	push()
	rotate(r)
	if(sx>=0)st+=(2-st)*.3
	for(var x=0;x<7;x++)for(var y=0;y<7;y++){
		fill(40)
		var targetable=(sx>-1)&&(sx<7)&&board[sx][sy]!=null&&abs(x-sx)+abs(y-sy)==1&&board[x][y]==null
		var selected=x==sx&&y==sy
		if((x==2||x==4)&&(y==2||y==4))fill(240)
		if((x==1||x==3||x==5)&&(y==2||y==4))fill(140)
		if((y==1||y==3||y==5)&&(x==2||x==4))fill(140)
		rect(x*10-30,y*10-30,10,10)
		if(board[x][y])fill([20,150,225][board[x][y].color%3],[20,150,225][Math.floor(board[x][y].color/3)%3],[20,150,225][Math.floor(board[x][y].color/9)%3],200+st*selected*35)
		if(board[x][y])rect(x*10-30,y*10-30,7+selected*st,7+selected*st)
		fill(255,128,0)
		if(board[x][y]&&board[x][y].checked)rect(x*10-30,y*10-30,2,2)
		fill(0,128,255)
		if(board[x][y]&&board[x][y].mated)rect(x*10-30,y*10-30,2,2)
		if(targetable)fill((board[sx][sy].color%3)*128,(Math.floor(board[sx][sy].color/3)%3)*128,(Math.floor(board[sx][sy].color/9)%3)*128,st*50)
		if(targetable)rect(x*10-30,y*10-30,st*3,st*3)
	}
	pop()
}
function keyPressed(){
	if(key=="r")socket.emit("reset",{})
	if(key==" ")socket.emit("commit",{})
}
function mousePressed(){
	var r2=map(tr%(2*PI),0,PI,1,-1)
	if(draw==game){
		if(sx>=0&&sx<=7){
			socket.emit("move",{x2:Math.round(r2*(mouseX-width/2)/(height/10)+3),y2:Math.round(r2*(mouseY-height/2)/(height/10)+3),x:sx,y:sy})
		}
		st=0
		sx=Math.round(r2*(mouseX-width/2)/(height/10)+3)
		sy=Math.round(r2*(mouseY-height/2)/(height/10)+3)
	}else if(draw==colorselect){
		st=0
		mx=(mouseX-width/2)/(height/100)
		my=(mouseY-height/2)/(height/100)
		var si2=Math.round(mx/10)*3+Math.round(my/10)+13
		if(abs((mx+55)%10-5)>3.5||abs((my+55)%10-5)>3.5||mx>45||mx<-45||my>15||my<-15)si=undefined
		else if(si==si2)si=undefined
		else if(si==undefined)si=si2
		else{
			socket.emit("join",{colors:[si,si2]})
			board[1][3]={color:si}
			board[3][5]={color:si}
			draw=game
		}
	}
}
